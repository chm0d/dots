#!/bin/bash

wp_path=/home/rich/Pictures/
image=$(ls $wp_path | grep -E '(jpg|png)$' | sort -R | tail -1)
wpg -s $wp_path/$image
wal -i $wp_path/$image
#echo 'export $wp_path/$image' >> ~/.bashrc
sleep 3
~/alacritty-color-export/script.sh ~/.config/alacritty/alacritty.yml 

