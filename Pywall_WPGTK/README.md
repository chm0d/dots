## How I get my colors ##

In this tutorial I will try to explain my setup of pywal and wpgtk.  For this tutorial
you will need [Pywal](https://github.com/dylanaraps/pywal) and [WPGTK](https://github.com/deviantfero/wpgtk).

For the longest time I wanted to be able to have my colors change by a push of a button as well as my gtk apps.  With this setup I have been able to accomplish what I have been wanting.

## Pywal ##

After installing Pywal open up a terminal and look at the help file to see what options are available:
```
[rich@archie dots]$ wal --help
usage: wal [-h] [-a "alpha"] [-b background] [--backend [backend]]
           [--theme [/path/to/file or theme_name]] [--iterative] [--saturate 0.0-1.0]
           [--preview] [--vte] [-c] [-i "/path/to/img.jpg"] [-l] [-n]
           [-o "script_name"] [-q] [-r] [-R] [-s] [-t] [-v] [-e]

wal - Generate colorschemes on the fly

optional arguments:
  -h, --help            show this help message and exit
  -a "alpha"            Set terminal background transparency. *Only works in URxvt*
  -b background         Custom background color to use.
  --backend [backend]   Which color backend to use. Use 'wal --backend' to list
                        backends.
  --theme [/path/to/file or theme_name], -f [/path/to/file or theme_name]
                        Which colorscheme file to use. Use 'wal --theme' to list
                        builtin themes.
  --iterative           When pywal is given a directory as input and this flag is
                        used: Go through the images in order instead of shuffled.
  --saturate 0.0-1.0    Set the color saturation.
  --preview             Print the current color palette.
  --vte                 Fix text-artifacts printed in VTE terminals.
  -c                    Delete all cached colorschemes.
  -i "/path/to/img.jpg"
                        Which image or directory to use.
  -l                    Generate a light colorscheme.
  -n                    Skip setting the wallpaper.
  -o "script_name"      External script to run after "wal".
  -q                    Quiet mode, don't print anything.
  -r                    'wal -r' is deprecated: Use (cat ~/.cache/wal/sequences &)
                        instead.
  -R                    Restore previous colorscheme.
  -s                    Skip changing colors in terminals.
  -t                    Skip changing colors in tty.
  -v                    Print "wal" version.
  -e                    Skip reloading gtk/xrdb/i3/sway/polybar

  ```



  I have only ever really used 2 options and those are -i and -r, which as you can see is deprecated now and replaced with ~/.cache/wal/sequences &.  You can place this in your ~/.xinitrc or ~/.xprofile to restore your previous wallpaper.  Since I use a display manager all my startup programs are in ~/.xprofile.  You can also put this in your ~/.bashrc file to retain the colors in your terminal.  For those of you who use Alacritty like myself, I found a script you can run that will allow the colors to remain.  It says it is for MACS but I am using it without any issues.  I couldn't get Alacritty to retain the color scheme without it.  The script is on my [Alacritty_Color_Export](./Pywal_WPGTK/script.sh).  Now I also created a small script to accomplish all this within one push of a button, which is also in my gitlab:

  ```
  1 #!/bin/bash
 2
 3 wp_path=/home/rich/Pictures/
 4 image=$(ls $wp_path | grep -E '(jpg|png)$' | sort -R | tail -1)
 5 wpg -s $wp_path/$image
 6 wal -i $wp_path/$image
 7 #echo 'export $wp_path/$image' >> ~/.bashrc
 8 sleep 3
 9 ~/alacritty-color-export/script.sh ~/.config/alacritty/alacritty.yml

```
Obviously change the path to your wallpapers and bind a key using sxhkd.

  ## WPGTK ##

  The only reason I use WPGTK is to set the colors of my gtk apps to coincide with my wallpaper.  I don't think you can do that with just Pywal alone.  One thing to know to use WPGTK to change your GTK apps colors your icon theme must be set to FlattColor.

  WPGTK can be used both in the terminal as well as a GUI.  When I change my wallpaper I just open up WPGTK and change to the same wallpaper I used with Pywal and it sets my colors accordingly.  Before you get this going you have to create a couple templates for WPGTK.  To do this open up a terminal and see what options are available to you for WPGTK:
  ```
  [rich@archie dots]$ wpg --help
usage: wpg [-h] [--version] [-a A [A ...]] [-d D [D ...]] [-t] [-s S [S ...]] [-l] [-n]
           [-m] [-c] [-z Z [Z ...]] [-A A [A ...]] [-r] [-L] [--theme [THEME]] [-T]
           [-i I I] [-o O [O ...]] [-R R [R ...]] [--link LINK LINK] [--sat SAT SAT]
           [--brt BRT BRT] [--backend [BACKEND]] [--alpha ALPHA] [--preview]
           [--update UPDATE [UPDATE ...]] [--noreload]

optional arguments:
  -h, --help            show this help message and exit
  --version             print the current version
  -a A [A ...]          add a wallpaper and generate a colorscheme
  -d D [D ...]          delete the wallpaper(s) from wallpaper folder
  -t                    add, remove and list templates instead of themes
  -s S [S ...]          set the wallpaper and/or colorscheme
  -l                    see which wallpapers are available
  -n                    avoid setting a wallpaper
  -m                    pick a random wallpaper/colorscheme
  -c                    shows the current wallpaper
  -z Z [Z ...]          shuffles the given colorscheme(s)
  -A A [A ...]          auto-adjusts the given colorscheme(s)
  -r                    restore the wallpaper and colorscheme
  -L, --light           temporarily enable light themes
  --theme [THEME]       list included pywal themes or replace your current colorscheme
                        with a selection of your own
  -T                    assign a pywal theme to a specific wallpaper instead of a json
                        file
  -i I I                import a theme in json format and assign to a wallpaper
                        [wallpaper, json]
  -o O [O ...]          export a theme in json format [wallpaper, json]
  -R R [R ...]          reset template(s) to their original colors
  --link LINK LINK      link config file to template backup [.base, config]
  --sat SAT SAT         add or substract the saturation of a colorscheme [colorscheme,
                        sat] (0, 1)
  --brt BRT BRT         add or substract the brightness of a colorscheme [colorscheme,
                        brt] (0, 255)
  --backend [BACKEND]   select a temporary backend
  --alpha ALPHA         set a one time alpha value
  --preview             preview your current colorscheme
  --update UPDATE [UPDATE ...]
                        update template(s) of your choice to the new format
  --noreload            Skip reloading other software afterapplying colorscheme

```

Before you do this you actually need to download the templates for WPGTK:

```
[rich@archie dots]$ wpg-install.sh -h
Usage :  /usr/bin/wpg-install.sh [options] [--]

  Options:
  -h   Display this message
  -v   Display script version
  -o   Install openbox templates
  -t   Install tint2 template
  -g   Install gtk template
  -i   Install icon-set
  -r   Install rofi template
  -I   Install i3 template
  -p   Install polybar template
  -b   Install bspwm template
  -d   Install dunst template
  -H   Specify hash of wpgtk-templates repository to use

  ```
  So from above you decide what applications you want templates for. I am only using the rofi, and gtk templates.  In order to get the templates you want you would type:
  ```
  wpg-install -rg (I am only installing rofi and gtk in this example)
  ```
  For warning if you already have configs for these make backup copies before you install the templates because it will over-ride them.  WPGTK will make backups by default but I like to always do it myself that way I know for sure there is a backup file.  After you have installed the templates you want to use you can open up WPGTK.  The command if you are doing it from the terminal would be just wpg.  This is what mine looks like after adding Rofi, and GTK templates:

  ![WPG](./wpg.png)

  Now what I change the wallpaper using pyway I open up WPGTK and set the exact wallpaper and click on set as shown here:

  ![WPG2](./wpg2.png)

  So from this point here if everything has been setup correctly your colors have changed in your GTK applications.

  Here are a few screenshots of my desktop:

  ![SS1](./ss1.png)

  ![SS2](./ss2.png)

  ![SS3](./ss3.png)
  
