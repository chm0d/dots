First and foremost credit for this ROFI theme goes to [Murzchnvok](https://github.com/Murzchnvok/polybar-fox)

The following screenshot is what this ROFI theme will look like:

![FOX](/rofi/fox-rofi-theme/fox.png)
