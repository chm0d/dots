## MPD + NCMPCPP with album art ##

After finishing this tutorial your NCMPCPP should look like this minus the colors.  The colors I have are part of pywal<br/>
and my wallpaper.  I will do a tutorial on that as well, but first lets get this NCMPCPP with album art going!

![alt text](./SSofNCMCPP.png)

The dependencies for this are urxvt with pixbuf enabled.  You will know you have pixbuf enabled if you can view images <br/>
within urxvt.  You can test your urxvt if pixbuf is enabled by trying the following </br>
```

urxvt -pixmap /path/to/your/image


```

You Will also need, obviously, NCMPCPP, W3M, which is the backend for urxvt.

There are also three files you will need that are on my [Github.](https://gitlab.com/chm0d/dots/-/tree/master/NCMPCPP_with_AlbumArt) You can git clone these files by typing in the terminal </br>
git clone https://gitlab.com/chm0d/dots/-/tree/master/NCMPCPP_with_AlbumArt.  Once you have done that you need to make NCMPCPP2 </br>
and albumart executable by typing in the console chmod +x NCMPCPP2 albumart.  Now place NCMPCPP2 into ~/.local.bin and albumart, and the config file to ~/.ncmcpp </br>
edit albumart and ncmpcpp_albumart to point to the respectful files and music directory.  If you open them both up in a text editor or VIM you will see </br>
where you will need to edit because right now they are pointing to my config file and music directory.

If you prefer to just copy the config and executable from here I will add them below</br>
Here is the executable NCMPCPP2:
```
#!/bin/bash
urxvt -g 70x10 -e ncmpcpp -c ~/.ncmpcpp/ncmpcpp-albumart
exit 0
```
And the config file ncmpcpp_albumart:
```
% egrep -v '^#' .ncmpcpp/config
mpd_music_dir = "~/NAS/Music/"

mpd_host = "127.0.0.1"
mpd_port = "6600"
mouse_list_scroll_whole_page = "yes"
lines_scrolled = "1"

visualizer_in_stereo = "no"
visualizer_fifo_path = "/tmp/mpd.fifo"
visualizer_output_name = "my_fifo"
visualizer_sync_interval = "10"
visualizer_type = "spectrum"

visualizer_look = "▋▋"
#visualizer_look = "█▋"

message_delay_time = "3"
playlist_shorten_total_times = "yes"
playlist_display_mode = "classic"
browser_display_mode = "columns"
search_engine_display_mode = "columns"
playlist_editor_display_mode = "columns"
autocenter_mode = "yes"
centered_cursor = "yes"
user_interface = "classic"
follow_now_playing_lyrics = "yes"
locked_screen_width_part = "60"
display_bitrate = "no"
external_editor = "nano"
main_window_color = default
main_window_highlight_color = 7

progressbar_elapsed_color = 7
progressbar_color = "black"

progressbar_look = "▃▃▃"
#progressbar_look = "─⊙ "

execute_on_song_change="~/.ncmpcpp/albumart"

mouse_support = "yes"
header_visibility = "no"
statusbar_visibility = "no"

statusbar_color = "white"
#visualizer_color = 2,3,4,5,6,7,8
visualizer_color = "white"
titles_visibility = "no"
enable_window_title = "yes"

now_playing_prefix = " $b$7$6$9"
now_playing_suffix = "$/b$9"

song_columns_list_format = "(6)[]{} (25)[blue]{a} (34)[white]{t|f} (5f)[magenta]{l} (1)[]{}"

color1 = "white"
color2 = "blue"

song_list_format = " %t $R%a │                     "

song_status_format = "$b$7♫ $2%a $4⟫$3⟫ $8%t $4⟫$3⟫ $5%b "

song_window_title_format = "  {%a}  {%t}"

```
One thing I have noticed when I couldn't get it working was in the albumart executable it is searching for cover.jpg </br>
and not Cover.jpg.  Notice the capitalization. I wasn't consistent in naming my albumart so pay attention to that if </br>
things are not working for you.  Something so easy tripped me up some.  Hopefully this has helped anyone wanting to </br>
configure their ncmpcpp with albumart. Keep in mind also how you have urxvt configured in your .bashrc will play a </br>
role in how it looks.  I use alacritty as my terminal and ncmpcpp with albumart with urxvt.
