Here is another github page that I found while googling tint2 and github.  As always credit </br>
goes torwards the OP, which you can find here.  [fikriomar16](https://github.com/fikriomar16/Tint2-Collections)</br>
Again I will post here in case you do not want to go to github:

## Preview
## Minimalist
![Minimalist](/Tint2-Collections/fikriomar16/Minimalist/2017-12-30-035802_1366x768_scrot.png)

## PowerArc
![PowerArc](/Tint2-Collections/fikriomar16/PowerArc/PowerArc.png)

## PowerArcDark
![PowerArcDark](/Tint2-Collections/fikriomar16/PowerArc/PowerArcDark.png)

## PolyTint2
![PolyTint2](/Tint2-Collections/fikriomar16/PolyTint2/PolyTint2.png)

## Neomix
![Neomix](/Tint2-Collections/fikriomar16/Neomix/2017-07-15-135256_1366x768_scrot.png)

## Opolah
![Opolah](/Tint2-Collections/fikriomar16/Opolah/2017-09-26-205558_1366x768_scrot.png)

## Pencar
![Pencar](/Tint2-Collections/fikriomar16/Pencar/2017-09-14-000224_1366x768_scrot.png)

## Stolen from m4he(DeviantArt)
![Stolen from m4he](/Tint2-Collections/fikriomar16/Stolen%20from%20m4he/2017-11-30-113427_1366x768_scrot.png)

## Stolen from n0ym4y(DeviantArt)
![Stolen from n0ym4y](/Tint2-Collections/fikriomar16/Stolen%20from%20n0ym4y/2017-11-18-073520_1366x768_scrot.png)

[LICENSE](/Tint2-Collections/fikriomar16/LICENSE.md)
